/*
    Cryptopp-qt
    A simple cryptography examples from crypto++ (https://github.com/weidai11/cryptopp)
    Copyright (C) 2020  Abrar

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <QWidget>

QT_BEGIN_NAMESPACE
namespace Ui { class crypto; }
QT_END_NAMESPACE

class crypto : public QWidget
{
    Q_OBJECT

public:
    crypto(QWidget *parent = nullptr);
    ~crypto();

private slots:
    void on_types_currentIndexChanged(int index);

    void on_plaintext_textChanged(const QString &arg1);

    void on_encrypt_clicked(bool checked);

    void on_decrypt_clicked(bool checked);

private:
    Ui::crypto *ui;

    std::string hashToString(unsigned char hash[], int len);

    std::string toMD5Hash(std::string plaintext);
    std::string toSHA512Hash(std::string plaintext);
    std::string toAESEncrypt(std::string plaintext);
    std::string toAESDecrypt(std::string ciphertext);
};
