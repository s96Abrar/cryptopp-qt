# Crypto

Some cryptography example from [crypto++](https://github.com/weidai11/cryptopp)

# Dependency

* crypto++

# Links

Some useful links.

* https://stackoverflow.com/questions/12306956/example-of-aes-using-crypto
* https://stackoverflow.com/questions/2262386/generate-sha256-with-openssl-and-c

