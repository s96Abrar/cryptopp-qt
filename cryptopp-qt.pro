QT       += widgets

TEMPLATE = app
TARGET = cryptopp-qt

CONFIG += c++11

LIBS += -lcryptopp

unix {
        isEmpty(PREFIX) {
                PREFIX = /usr
        }

        target.path	  = $$PREFIX/bin/
        INSTALLS	 += target
}

SOURCES += \
    main.cpp \
    crypto.cpp

HEADERS += \
    crypto.h

FORMS += \
    crypto.ui
