/*
    Cryptopp-qt
    A simple cryptography examples from crypto++ (https://github.com/weidai11/cryptopp)
    Copyright (C) 2020  Abrar

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <cryptopp/aes.h>
#include <cryptopp/md5.h>
#include <cryptopp/sha.h>
#include <cryptopp/modes.h>
#include <cryptopp/filters.h>

#include "crypto.h"
#include "ui_crypto.h"

crypto::crypto(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::crypto)
{
    ui->setupUi(this);
    ui->decrypt->setVisible(0);
}

crypto::~crypto()
{
    delete ui;
}

std::string crypto::hashToString(unsigned char hash[], int len)
{
    char *str = reinterpret_cast<char*>(hash);
    char output[len * 2 + 1];
    for (int i = 0; i < (int)strlen(str); i++) {
        sprintf(output + (i * 2), "%02x", hash[i]);
    }

    output[len * 2] = 0;
    return std::string(output);
}

std::string crypto::toMD5Hash(std::string plaintext)
{
    int len = plaintext.size();
    const unsigned char *in = reinterpret_cast<const unsigned char *>(plaintext.data());

    CryptoPP::MD5 md5;
    unsigned char *out = new unsigned char(CryptoPP::MD5::DIGESTSIZE);
    md5.CalculateDigest(out, in, len);
    return (hashToString(out, CryptoPP::MD5::DIGESTSIZE));
}

std::string crypto::toSHA512Hash(std::string plaintext)
{
    int len = plaintext.size();
    const unsigned char *in = reinterpret_cast<const unsigned char *>(plaintext.data());

    CryptoPP::SHA512 sha512;
    unsigned char *out = new unsigned char(CryptoPP::SHA512::DIGESTSIZE);
    sha512.CalculateDigest(out, in, len);
    return (hashToString(out, CryptoPP::SHA512::DIGESTSIZE));
}

std::string crypto::toAESEncrypt(std::string plaintext)
{
    std::string ciphertext;

    // Key and IV setup
    // AES encryption uses a secret key of a variable length (128-bit, 196-bit or 256-bit).
    // This key is secretly exchanged between two parties before communication begins.
    // DEFAULT_KEYLENGTH = 16 bytes
    CryptoPP::byte key[ CryptoPP::AES::DEFAULT_KEYLENGTH ], iv[ CryptoPP::AES::BLOCKSIZE ];
    memset(key, 0x00, CryptoPP::AES::DEFAULT_KEYLENGTH);
    memset(iv, 0x00, CryptoPP::AES::BLOCKSIZE);

    CryptoPP::AES::Encryption aesEncryption(key, CryptoPP::AES::DEFAULT_KEYLENGTH);
    CryptoPP::CBC_Mode_ExternalCipher::Encryption cbcEncryption(aesEncryption, iv);

    CryptoPP::StreamTransformationFilter stfEncryptor(cbcEncryption, new CryptoPP::StringSink(ciphertext));
    stfEncryptor.Put(reinterpret_cast<const unsigned char *>(plaintext.c_str()), plaintext.length());
    stfEncryptor.MessageEnd();

    char outputBuffer[ciphertext.size() * 2 + 1];
    for (int i = 0; i < (int)ciphertext.size(); i++) {
        sprintf(outputBuffer + (i * 2), "%02x", ciphertext[i]);
    }

    ciphertext = std::string(outputBuffer);

    return ciphertext;
}

std::string crypto::toAESDecrypt(std::string ciphertext)
{
    std::string plaintext;

    // Key and IV setup
    // AES encryption uses a secret key of a variable length (128-bit, 196-bit or 256-bit).
    // This key is secretly exchanged between two parties before communication begins.
    // DEFAULT_KEYLENGTH = 16 bytes
    CryptoPP::byte key[ CryptoPP::AES::DEFAULT_KEYLENGTH ], iv[ CryptoPP::AES::BLOCKSIZE ];
    memset(key, 0x00, CryptoPP::AES::DEFAULT_KEYLENGTH);
    memset(iv, 0x00, CryptoPP::AES::BLOCKSIZE);

    CryptoPP::AES::Decryption aesDecryption(key, CryptoPP::AES::DEFAULT_KEYLENGTH);
    CryptoPP::CBC_Mode_ExternalCipher::Decryption cbcDecryption(aesDecryption, iv);

    CryptoPP::StreamTransformationFilter stfDecryptor(cbcDecryption, new CryptoPP::StringSink(plaintext));
    stfDecryptor.Put(reinterpret_cast<const unsigned char *>(ciphertext.c_str()), ciphertext.size());
    stfDecryptor.MessageEnd();

    return plaintext;
}

void crypto::on_types_currentIndexChanged(int index)
{
    if (index == 2) {
        ui->decrypt->setVisible(1);
    } else {
        ui->decrypt->setVisible(0);
    }

    on_plaintext_textChanged(ui->plaintext->text());
}

void crypto::on_plaintext_textChanged(const QString &arg1)
{
    if (!arg1.count()) {
        return;
    }

    if (ui->encrypt->isChecked()) {
        switch (ui->types->currentIndex()) {
        case 0: // MD5
            ui->ciphertext->setText(
                        QString::fromStdString(
                            toMD5Hash(
                                arg1.toStdString())));
            break;
        case 1: // SHA512
            ui->ciphertext->setText(
                        QString::fromStdString(
                            toSHA512Hash(
                                arg1.toStdString())));
            break;
        case 2: // AES
            ui->ciphertext->setText(
                        QString::fromStdString(
                            toAESEncrypt(
                                arg1.toStdString())));
            break;
        }
    } else {
        ui->ciphertext->setText(
                    QString::fromStdString(
                        toAESDecrypt(
                            arg1.toStdString())));
    }
}

void crypto::on_encrypt_clicked(bool checked)
{
    Q_UNUSED(checked)
    on_plaintext_textChanged(ui->plaintext->text());
}

void crypto::on_decrypt_clicked(bool checked)
{
    Q_UNUSED(checked);
    on_plaintext_textChanged(ui->plaintext->text());
}
